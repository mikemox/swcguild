			 function loadingPage() {
				document.getElementsByClassName("noMoney")[0].style.visibility = "hidden";
				document.getElementsByClassName("noMoney")[1].style.visibility = "hidden";
				alert("Welcome to my Lucky Sevens game! \n \n "
				+ "Please enter a 'whole' dollar amount to bet and click play to begin rolling the dice.\n "
				+ "The rules are as follow: \n "
				+ "     1. Each round, the program rolls a virtual pair of dice for the user. \n "
				+ "     2. If the sum of the 2 dice is equal to 7, the player wins $4; \n "
				+ "        otherwise, the player loses $1. \n \n "
				+ "                                  Good luck and have fun!"); 
			};
			function unloadingPage() {
				document.getElementsByClassName("noMoney")[0].style.visibility = "visible";
				document.getElementsByClassName("noMoney")[1].style.visibility = "visible";
				document.getElementById("playButton").textContent = "Play Again";
			}; 
						 

			function rollDice(){
				var myArray = [];
				var maxMoney = document.getElementById("maxMoney");
				var maxRolls = document.getElementById("maxRolls");
				var d1 = Math.floor(Math.random() * 6) + 1;
				var d2 = Math.floor(Math.random() * 6) + 1;
				var diceTotal = d1 + d2;
				var rollCount = 0;
				var input = document.getElementById("userInput").value;
				console.log("You have bet "+input+" dollars.");
				document.getElementById("startingBet").innerHTML = "$"+input;
				document.getElementById("maxMoney").textContent ="$"+input;
				document.getElementById("maxRolls").textContent = 0;
				maxMoney = input;
				while(input > 0) {
					rollCount++;
					
					if (diceTotal == 7){	
						input = (input - 1) + 5;	
						console.log("You rolled "+diceTotal+" You Win! Amount remaining is: $" +input+".");
						
						if(input > maxMoney){
							document.getElementById("maxMoney").textContent = "$"+input;
							document.getElementById("maxRolls").textContent = rollCount;
							maxMoney = input;
							var d1 = Math.floor(Math.random() * 6) + 1;
							var d2 = Math.floor(Math.random() * 6) + 1;
							var diceTotal = d1 + d2;							
						}
						else{
							
							var d1 = Math.floor(Math.random() * 6) + 1;
							var d2 = Math.floor(Math.random() * 6) + 1;
							var diceTotal = d1 + d2;
						}
						
						/*If the last two values do not change then the Highest Amount Held
						was the starting Bet, and that roll was zero. */
					}					
					else{
						
						input -= 1;
						console.log("You rolled "+diceTotal+" You Lose! Amount remaining: $ "+input+".");
						var d1 = Math.floor(Math.random() * 6) + 1;
						var d2 = Math.floor(Math.random() * 6) + 1;
						var diceTotal = d1 + d2;
						
					}
					
					document.getElementById("totalRolls").textContent = rollCount;
				}	
					unloadingPage();
					
					var answer = prompt("If you like to play again type 'yes' to refresh the page. \n " 
						+"If you would like to stay on the current page and keep your data select, 'no'.","Type: yes , no");
					
					switch(answer) {
						case 'yes':
							location.reload();
							break;
						case 'no':
							alert("Thank you for playing, I will leave your results on this page.");
							break;
						default:
							answer = alert("Invalid response. Refresh the page to play again.");
							break;
						}
				
				
				
				
				
			};